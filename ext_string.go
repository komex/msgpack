// Code generated by "stringer -type=Ext -linecomment"; DO NOT EDIT.

package msgpack

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[ExtTimestamp - -1]
}

const _Ext_name = "Timestamp"

var _Ext_index = [...]uint8{0, 9}

func (i Ext) String() string {
	i -= -1
	if i < 0 || i >= Ext(len(_Ext_index)-1) {
		return "Ext(" + strconv.FormatInt(int64(i+-1), 10) + ")"
	}
	return _Ext_name[_Ext_index[i]:_Ext_index[i+1]]
}
