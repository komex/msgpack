// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

import (
	"encoding/binary"
	"errors"
	"fmt"
	"math"
	"reflect"
	"time"
	"unsafe"
)

// Encoder allows to encode and write msgpack values to Writer.
type Encoder struct {
	w   Writer
	buf []byte
}

// NewEncoder returns a new instance of Encoder.
func NewEncoder(w Writer) *Encoder {
	// Size of buffer is sum of max int size + one Format byte.
	return &Encoder{w: w, buf: make([]byte, bits128)}
}

// Reset resets data destination.
func (e *Encoder) Reset(w Writer) {
	e.w = w
}

// Uint8 encodes uint8 as msgpack integer.
func (e *Encoder) Uint8(v uint8) error {
	if Format(v) <= FormatPositiveFixIntMax {
		if err := e.w.WriteByte(v); err != nil {
			return fmt.Errorf("write positive fix int: %w", err)
		}

		return nil
	}

	buf := e.buf[:2]
	buf[0], buf[1] = byte(FormatUint8), v

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Uint16 encodes uint16 as msgpack integer.
func (e *Encoder) Uint16(v uint16) error {
	if v <= math.MaxUint8 {
		return e.Uint8(uint8(v))
	}

	buf := e.buf[:1]
	buf[0] = byte(FormatUint16)
	buf = binary.BigEndian.AppendUint16(buf, v)

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Uint32 encodes uint32 as msgpack integer.
func (e *Encoder) Uint32(v uint32) error {
	if v <= math.MaxUint8 {
		return e.Uint8(uint8(v))
	}

	buf := e.buf[:1]

	if v <= math.MaxUint16 {
		buf[0] = byte(FormatUint16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(v))
	} else {
		buf[0] = byte(FormatUint32)
		buf = binary.BigEndian.AppendUint32(buf, v)
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Uint64 encodes uint64 as msgpack integer.
func (e *Encoder) Uint64(v uint64) error {
	if v <= math.MaxUint8 {
		return e.Uint8(uint8(v))
	}

	buf := e.buf[:1]

	switch {
	case v <= math.MaxUint16:
		buf[0] = byte(FormatUint16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(v))
	case v <= math.MaxUint32:
		buf[0] = byte(FormatUint32)
		buf = binary.BigEndian.AppendUint32(buf, uint32(v))
	default:
		buf[0] = byte(FormatUint64)
		buf = binary.BigEndian.AppendUint64(buf, v)
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Uint encodes uint as msgpack integer.
func (e *Encoder) Uint(v uint) error {
	if unsafe.Sizeof(v) == bits32 {
		return e.Uint32(uint32(v))
	}

	return e.Uint64(uint64(v))
}

// Int8 encodes int8 as msgpack integer.
func (e *Encoder) Int8(v int8) error {
	if v >= 0 {
		return e.Uint8(uint8(v))
	}

	return e.int8(v)
}

// Int16 encodes int16 as msgpack integer.
func (e *Encoder) Int16(v int16) error {
	if v >= 0 {
		return e.Uint16(uint16(v))
	}

	return e.int16(v)
}

// Int32 encodes int32 as msgpack integer.
func (e *Encoder) Int32(v int32) error {
	if v >= 0 {
		return e.Uint32(uint32(v))
	}

	return e.int32(v)
}

// Int64 encodes int64 as msgpack integer.
func (e *Encoder) Int64(v int64) error {
	if v >= 0 {
		return e.Uint64(uint64(v))
	}

	return e.int64(v)
}

// Int encodes int as msgpack integer.
func (e *Encoder) Int(v int) error {
	return e.Int64(int64(v))
}

// MapLen encodes map length as msgpack.
func (e *Encoder) MapLen(length int) error {
	return e.structLen(length, FormatMap16, FormatMap32, FormatFixMapMin, FormatFixMapMax)
}

// ArrayLen encodes array length as msgpack.
func (e *Encoder) ArrayLen(length int) error {
	return e.structLen(length, FormatArray16, FormatArray32, FormatFixArrayMin, FormatFixArrayMax)
}

// String encodes v as msgpack string.
func (e *Encoder) String(v string) error {
	b := e.strToBytes(v)

	if len(v) <= int(FormatFixStringMax-FormatFixStringMin) {
		if err := e.w.WriteByte(byte(FormatFixStringMin) + byte(len(v))); err != nil {
			return fmt.Errorf("write fix string length: %w", err)
		}

		if _, err := e.w.Write(b); err != nil {
			return fmt.Errorf("write fix string: %w", err)
		}

		return nil
	}

	return e.bin(FormatString8, FormatString16, FormatString32, b)
}

// Bytes encodes v as msgpack bin data.
func (e *Encoder) Bytes(v []byte) error {
	return e.bin(FormatBin8, FormatBin16, FormatBin32, v)
}

// Float32 encodes v as msgpack value.
func (e *Encoder) Float32(v float32) error {
	buf := e.buf[:1]
	buf[0] = byte(FormatFloat32)
	buf = binary.BigEndian.AppendUint32(buf, math.Float32bits(v))

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Float64 encodes v as msgpack value.
func (e *Encoder) Float64(v float64) error {
	buf := e.buf[:1]

	buf[0] = byte(FormatFloat64)
	buf = binary.BigEndian.AppendUint64(buf, math.Float64bits(v))

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

// Bool encodes v as msgpack value.
func (e *Encoder) Bool(v bool) error {
	if v {
		return e.format(FormatTrue)
	}

	return e.format(FormatFalse)
}

// Nil encodes v as msgpack value.
func (e *Encoder) Nil() error {
	return e.format(FormatNil)
}

// ExtData encodes data with specified Ext code.
func (e *Encoder) ExtData(code Ext, data []byte) error {
	// There are fixed sizes described in msgpack protocol.
	const (
		FixedSize1 = 1 << iota
		FixedSize2
		FixedSize4
		FixedSize8
		FixedSize16
	)

	switch len(data) {
	case FixedSize1:
		return e.extDataFixed(FormatFixExt1, code, data)
	case FixedSize2:
		return e.extDataFixed(FormatFixExt2, code, data)
	case FixedSize4:
		return e.extDataFixed(FormatFixExt4, code, data)
	case FixedSize8:
		return e.extDataFixed(FormatFixExt8, code, data)
	case FixedSize16:
		return e.extDataFixed(FormatFixExt16, code, data)
	}

	// Ext data with specified size.
	if err := e.binHeader(FormatExt8, FormatExt16, FormatExt32, len(data)); err != nil {
		return err
	}

	if err := e.w.WriteByte(byte(code)); err != nil {
		return fmt.Errorf("write ext header: %w", err)
	}

	if len(data) == 0 {
		return nil
	}

	if _, err := e.w.Write(data); err != nil {
		return fmt.Errorf("write ext data: %w", err)
	}

	return nil
}

// Time encoder time as msgpack value.
func (e *Encoder) Time(t time.Time) error {
	sec := t.Unix()
	nsec := int64(t.Nanosecond())
	buf := e.buf[:0]

	const shift = 34

	if (sec >> shift) == 0 {
		if nsec == 0 {
			buf = binary.BigEndian.AppendUint32(buf, uint32(sec))
		} else {
			buf = binary.BigEndian.AppendUint64(buf, uint64((nsec<<shift)|sec))
		}
	} else {
		buf = binary.BigEndian.AppendUint32(buf, uint32(nsec))
		buf = binary.BigEndian.AppendUint64(buf, uint64(sec))
	}

	if err := e.ExtData(ExtTimestamp, buf); err != nil {
		return fmt.Errorf("write time: %w", err)
	}

	return nil
}

// Interface encodes any scalar value or structure has implemented CustomEncoder as msgpack value.
func (e *Encoder) Interface(value any) error {
	if enc, ok := value.(CustomEncoder); ok {
		if err := enc.EncodeMsgpack(e); err != nil {
			return fmt.Errorf("custom encoder: %w", err)
		}

		return nil
	}

	if err := e.integer(value); !errors.Is(err, ErrUnsupportedType) {
		return err
	}

	if err := e.simpleElement(value); !errors.Is(err, ErrUnsupportedType) {
		return err
	}

	v := reflect.ValueOf(value)
	switch v.Kind() {
	case reflect.Ptr:
		return e.Interface(v.Elem().Interface())
	case reflect.Slice, reflect.Array:
		return e.encodeArray(v)
	case reflect.Map:
		return e.encodeMap(v)
	default:
		return ErrUnsupportedType
	}
}

func (e *Encoder) integer(value any) error {
	switch v := value.(type) {
	case int:
		return e.Int(v)
	case uint:
		return e.Uint(v)
	case uint8:
		return e.Uint8(v)
	case uint16:
		return e.Uint16(v)
	case uint32:
		return e.Uint32(v)
	case uint64:
		return e.Uint64(v)
	case int8:
		return e.Int8(v)
	case int16:
		return e.Int16(v)
	case int32:
		return e.Int32(v)
	case int64:
		return e.Int64(v)
	}

	return ErrUnsupportedType
}

func (e *Encoder) simpleElement(value any) error {
	switch v := value.(type) {
	case string:
		return e.String(v)
	case []byte:
		return e.Bytes(v)
	case float32:
		return e.Float32(v)
	case float64:
		return e.Float64(v)
	case nil:
		return e.Nil()
	case bool:
		return e.Bool(v)
	}

	return ErrUnsupportedType
}

func (e *Encoder) encodeArray(v reflect.Value) error {
	elements := v.Len()
	if err := e.ArrayLen(elements); err != nil {
		return fmt.Errorf("encode array length: %w", err)
	}

	for i := range elements {
		if err := e.Interface(v.Index(i).Interface()); err != nil {
			return fmt.Errorf("encode array element: %w", err)
		}
	}

	return nil
}

func (e *Encoder) encodeMap(v reflect.Value) error {
	if err := e.MapLen(v.Len()); err != nil {
		return fmt.Errorf("encode map length: %w", err)
	}

	for _, k := range v.MapKeys() {
		if err := e.Interface(k.Interface()); err != nil {
			return fmt.Errorf("encode map key: %w", err)
		}

		if err := e.Interface(v.MapIndex(k).Interface()); err != nil {
			return fmt.Errorf("encode map value: %w", err)
		}
	}

	return nil
}

func (e *Encoder) int8(v int8) error {
	var err error

	if Format(v) >= FormatNegativeFixIntMin {
		err = e.w.WriteByte(byte(v))
	} else {
		buf := e.buf[:2]
		buf[0], buf[1] = byte(FormatInt8), byte(v)
		_, err = e.w.Write(buf)
	}

	if err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

func (e *Encoder) int16(v int16) error {
	if v >= math.MinInt8 {
		return e.int8(int8(v))
	}

	buf := e.buf[:1]
	buf[0] = byte(FormatInt16)
	buf = binary.BigEndian.AppendUint16(buf, uint16(v))

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

func (e *Encoder) int32(v int32) error {
	if v >= math.MinInt8 {
		return e.int8(int8(v))
	}

	buf := e.buf[:1]

	switch {
	case v >= math.MinInt16:
		buf[0] = byte(FormatInt16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(v))
	default:
		buf[0] = byte(FormatInt32)
		buf = binary.BigEndian.AppendUint32(buf, uint32(v))
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

func (e *Encoder) int64(v int64) error {
	if v >= math.MinInt8 {
		return e.int8(int8(v))
	}

	buf := e.buf[:1]

	switch {
	case v >= math.MinInt16:
		buf[0] = byte(FormatInt16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(v))
	case v >= math.MinInt32:
		buf[0] = byte(FormatInt32)
		buf = binary.BigEndian.AppendUint32(buf, uint32(v))
	default:
		buf[0] = byte(FormatInt64)
		buf = binary.BigEndian.AppendUint64(buf, uint64(v))
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write: %w", err)
	}

	return nil
}

func (e *Encoder) format(format Format) error {
	if err := e.w.WriteByte(byte(format)); err != nil {
		return fmt.Errorf("write format: %w", err)
	}

	return nil
}

func (e *Encoder) binHeader(format8, format16, format32 Format, length int) error {
	buf := make([]byte, 1, bits8+bits32)

	switch {
	case length <= math.MaxUint8:
		buf[0] = byte(format8)
		buf = append(buf, byte(length))
	case length <= math.MaxUint16:
		buf[0] = byte(format16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(length))
	case length <= math.MaxUint32:
		buf[0] = byte(format32)
		buf = binary.BigEndian.AppendUint32(buf, uint32(length))
	default:
		return ErrLengthTooLong
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write length: %w", err)
	}

	return nil
}

func (e *Encoder) bin(format8, format16, format32 Format, v []byte) error {
	l := len(v)
	if err := e.binHeader(format8, format16, format32, l); err != nil {
		return err
	}

	if l == 0 {
		return nil
	}

	if _, err := e.w.Write(v); err != nil {
		return fmt.Errorf("write data: %w", err)
	}

	return nil
}

// strToBytes converts string s to []byte faster than type casting.
func (*Encoder) strToBytes(s string) []byte {
	if s == "" {
		return nil
	}

	return unsafe.Slice(unsafe.StringData(s), len(s))
}

func (e *Encoder) structLen(length int, format16, format32, formatMin, formatMax Format) error {
	if length < 0 {
		return ErrNegativeLength
	}

	if length > math.MaxUint32 {
		return ErrLengthTooLong
	}

	if length <= int(formatMax-formatMin) {
		if err := e.w.WriteByte(byte(formatMin) + byte(length)); err != nil {
			return fmt.Errorf("write fix struct length: %w", err)
		}

		return nil
	}

	buf := e.buf[:1]

	if length <= math.MaxUint16 {
		buf[0] = byte(format16)
		buf = binary.BigEndian.AppendUint16(buf, uint16(length))
	} else {
		buf[0] = byte(format32)
		buf = binary.BigEndian.AppendUint32(buf, uint32(length))
	}

	if _, err := e.w.Write(buf); err != nil {
		return fmt.Errorf("write struct length: %w", err)
	}

	return nil
}

func (e *Encoder) extDataFixed(format Format, code Ext, data []byte) error {
	if err := e.w.WriteByte(byte(format)); err != nil {
		return fmt.Errorf("write format: %w", err)
	}

	if err := e.w.WriteByte(byte(code)); err != nil {
		return fmt.Errorf("write code: %w", err)
	}

	if len(data) == 0 {
		return nil
	}

	if _, err := e.w.Write(data); err != nil {
		return fmt.Errorf("write ext data: %w", err)
	}

	return nil
}
