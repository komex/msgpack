package msgpack

import (
	"fmt"
)

func read8[T int8 | uint8](d *Decoder) (T, error) {
	v, err := d.r.ReadByte()
	if err != nil {
		return 0, fmt.Errorf("read byte: %w", err)
	}

	return T(v), nil
}

func read16[T int16 | uint16](d *Decoder) (T, error) {
	v, err := d.read16()
	if err != nil {
		return 0, err
	}

	return T(v), nil
}

func read32[T int32 | uint32](d *Decoder) (T, error) {
	v, err := d.read32()
	if err != nil {
		return 0, err
	}

	return T(v), nil
}

func read64[T int64 | uint64](d *Decoder) (T, error) {
	v, err := d.read64()
	if err != nil {
		return 0, err
	}

	return T(v), nil
}
