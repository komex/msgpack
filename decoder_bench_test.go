package msgpack_test

import (
	"bytes"
	"errors"
	"io"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/komex/msgpack/v2"
)

func BenchmarkDecoder_Integer(b *testing.B) {
	// bufferElements sets the number of bytes repeated in buffer.
	const bufferElements = 100

	benchmarks := []struct {
		name string
		typ  msgpack.IntType
		fn   func(d *msgpack.Decoder) error
	}{
		{
			"Uint8",
			UInt8,
			func(d *msgpack.Decoder) error {
				_, err := d.Uint8()

				return err
			},
		},
		{
			"Uint16",
			UInt16,
			func(d *msgpack.Decoder) error {
				_, err := d.Uint16()

				return err
			},
		},
		{
			"Uint32",
			UInt32,
			func(d *msgpack.Decoder) error {
				_, err := d.Uint32()

				return err
			},
		},
		{
			"Uint64",
			UInt64,
			func(d *msgpack.Decoder) error {
				_, err := d.Uint64()

				return err
			},
		},
		{
			"Int8",
			Int8,
			func(d *msgpack.Decoder) error {
				_, err := d.Int8()

				return err
			},
		},
		{
			"Int16",
			Int16,
			func(d *msgpack.Decoder) error {
				_, err := d.Int16()

				return err
			},
		},
		{
			"Int32",
			Int32,
			func(d *msgpack.Decoder) error {
				_, err := d.Int32()

				return err
			},
		},
		{
			"Int64",
			Int64,
			func(d *msgpack.Decoder) error {
				_, err := d.Int64()

				return err
			},
		},
		{
			"Float32",
			Float32,
			func(d *msgpack.Decoder) error {
				_, err := d.Float32()

				return err
			},
		},
		{
			"Float64",
			Float64,
			func(d *msgpack.Decoder) error {
				_, err := d.Float64()

				return err
			},
		},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			for _, def := range NewDataDefList(bm.typ) {
				b.Run(def.Name, func(b *testing.B) {
					data := bytes.Repeat(def.Buf, bufferElements)
					buf := bytes.NewReader(data)

					d := msgpack.NewDecoder(buf)

					b.ReportAllocs()
					b.SetBytes(int64(len(def.Buf)))
					b.ResetTimer()

					for i := 0; i < b.N; i++ {
						err := bm.fn(d)
						require.NoError(b, err)

						if i%bufferElements == 0 {
							buf.Reset(data)
						}
					}
				})
			}
		})
	}
}

func BenchmarkDecoder_String(b *testing.B) {
	const count = 5

	buf := []byte{
		0xa0,
		0xa1, 0x61,
		0xd9, 0x02, 0x62, 0x63,
		0xda, 0x00, 0x03, 0x64, 0x65, 0x66,
		0xdb, 0x00, 0x00, 0x00, 0x04, 0x67, 0x68, 0x69, 0x70,
	}

	r := bytes.NewReader(buf)
	d := msgpack.NewDecoder(r)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := d.String()
		require.NoError(b, err)

		if i%count == 0 {
			r.Reset(buf)
		}
	}
}

func BenchmarkDecoder_Bytes(b *testing.B) {
	const count = 3

	buf := []byte{
		0xc4, 0x02, 0x62, 0x63,
		0xc5, 0x00, 0x03, 0x64, 0x65, 0x66,
		0xc6, 0x00, 0x00, 0x00, 0x04, 0x67, 0x68, 0x69, 0x70,
	}

	r := bytes.NewReader(buf)
	d := msgpack.NewDecoder(r)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := d.Bytes()
		require.NoError(b, err)

		if i%count == 0 {
			r.Reset(buf)
		}
	}
}

func BenchmarkDecoder_Skip(b *testing.B) {
	buf := []byte{
		0x82, 0xa4, 0x62, 0x6f, 0x6f, 0x6c, 0xc3, 0xa3, 0x6d,
		0x61, 0x70, 0x82, 0xa5, 0x61, 0x72, 0x72, 0x61, 0x79,
		0x93, 0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85,
		0x1f, 0xa2, 0x64, 0x64, 0xc0, 0xa6, 0x73, 0x74, 0x72,
		0x69, 0x6e, 0x67, 0xa3, 0x61, 0x62, 0x63,
	}
	r := bytes.NewReader(buf)
	d := msgpack.NewDecoder(r)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err := d.Skip()
		if err != nil {
			if errors.Is(err, io.EOF) {
				r.Reset(buf)

				continue
			}

			b.Fatal(err)
		}
	}
}

func BenchmarkDecoder_Array(b *testing.B) {
	b.ReportAllocs()

	benchmarks := []struct {
		name     string
		elements int
	}{
		{
			"zero",
			0,
		},
		{
			"1",
			1,
		},
		{
			"5",
			5,
		},
		{
			"10",
			10,
		},
		{
			"100",
			100,
		},
	}
	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			var buf bytes.Buffer
			enc := msgpack.NewEncoder(&buf)

			err := enc.ArrayLen(bm.elements)
			require.NoError(b, err)

			for i := range bm.elements {
				err = enc.Int(i)
				require.NoError(b, err)
			}

			r := bytes.NewReader(buf.Bytes())
			d := msgpack.NewDecoder(r)

			b.ReportAllocs()
			b.SetBytes(int64(buf.Len()))
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				arrayLen, err := d.ArrayLen()
				require.NoError(b, err)
				require.Equal(b, bm.elements, arrayLen)

				for range arrayLen {
					_, err = d.Int()
					require.NoError(b, err)
				}

				r.Reset(buf.Bytes())
			}
		})
	}
}

func BenchmarkDecoder_Map(b *testing.B) {
	b.ReportAllocs()

	benchmarks := []struct {
		name     string
		elements int
	}{
		{
			"zero",
			0,
		},
		{
			"1",
			1,
		},
		{
			"5",
			5,
		},
		{
			"10",
			10,
		},
		{
			"100",
			100,
		},
	}
	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			var buf bytes.Buffer
			enc := msgpack.NewEncoder(&buf)

			err := enc.MapLen(bm.elements)
			require.NoError(b, err)

			for i := range bm.elements {
				err = enc.Int(i)
				require.NoError(b, err)
				err = enc.Int(i)
				require.NoError(b, err)
			}

			r := bytes.NewReader(buf.Bytes())
			d := msgpack.NewDecoder(r)

			b.ReportAllocs()
			b.SetBytes(int64(buf.Len()))
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				arrayLen, err := d.MapLen()
				require.NoError(b, err)
				require.Equal(b, bm.elements, arrayLen)

				for range arrayLen {
					_, err = d.Int()
					require.NoError(b, err)
					_, err = d.Int()
					require.NoError(b, err)
				}

				r.Reset(buf.Bytes())
			}
		})
	}
}

func BenchmarkDecoder_Bool(b *testing.B) {
	b.ReportAllocs()

	benchmarks := []struct {
		name string
		data []byte
	}{
		{
			"true",
			[]byte{0xc3},
		},
		{
			"false",
			[]byte{0xc2},
		},
	}
	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			r := bytes.NewReader(bm.data)
			d := msgpack.NewDecoder(r)

			b.ReportAllocs()
			b.SetBytes(int64(len(bm.data)))
			b.ResetTimer()

			for i := 0; i < b.N; i++ {
				_, err := d.Bool()
				require.NoError(b, err)

				r.Reset(bm.data)
			}
		})
	}
}

func BenchmarkDecoder_Time(b *testing.B) {
	const count = 3

	buf := []byte{
		0xd6, 0xff, 0x5f, 0x1f, 0x0a, 0xea,
		0xd7, 0xff, 0x88, 0x4d, 0xcf, 0x44, 0x5f, 0x1f, 0x0a, 0xea,
		0xc7, 0x0c, 0xff, 0x22, 0x13, 0x73, 0xd1, 0x00, 0xb2, 0xea, 0xd0, 0xdc, 0x9e, 0xa6, 0x6a,
	}

	r := bytes.NewReader(buf)
	d := msgpack.NewDecoder(r)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_, err := d.Time()
		require.NoError(b, err)

		if i%count == 0 {
			r.Reset(buf)
		}
	}
}
