package msgpack

import (
	"fmt"
)

// UnexpectedExtensionError is an error for unexpected extension while decoding.
type UnexpectedExtensionError struct {
	Extension Ext
}

// NewUnexpectedExtensionError creates a new instance of UnexpectedExtensionError.
func NewUnexpectedExtensionError(extension Ext) *UnexpectedExtensionError {
	return &UnexpectedExtensionError{Extension: extension}
}

// Error implements error interface.
func (u UnexpectedExtensionError) Error() string {
	return fmt.Sprintf("unexpected extension %s", u.Extension)
}
