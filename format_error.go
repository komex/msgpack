package msgpack

import (
	"fmt"
)

// FormatError is a wrapper for discard format errors.
type FormatError struct {
	Format Format
	Err    error
}

// NewFormatError returns a new instance of FormatError.
func NewFormatError(format Format, err error) *FormatError {
	return &FormatError{Format: format, Err: err}
}

// Unwrap returns origin error.
func (e FormatError) Unwrap() error {
	return e.Err
}

// Error implements error interface.
func (e FormatError) Error() string {
	return fmt.Sprintf("format %s: %s", e.Format, e.Err)
}
