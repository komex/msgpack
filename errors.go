// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

import (
	"errors"
)

// Package level errors.
var (
	ErrNanosecondsTooLarge = errors.New("nanoseconds too large")
	ErrNegativeLength      = errors.New("length cannot be negative")
	ErrLengthTooLong       = errors.New("struct length is too long")
	ErrOverflow            = errors.New("value overflows int range")
	ErrUnexpectedFormat    = errors.New("unexpected format")
	ErrUnexpectedLength    = errors.New("unexpected length")
	ErrUnsupportedMapKey   = errors.New("decoded map key is unsupported")
	ErrUnsupportedType     = errors.New("value has unsupported type")
)
