// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

const (
	bits8 = 1 << iota
	bits16
	bits32
	bits64
	bits128
)
