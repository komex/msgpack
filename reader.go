package msgpack

import (
	"io"
)

// Reader describes interface required by Decoder.
type Reader interface {
	io.ByteScanner
	io.Reader
}
