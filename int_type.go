package msgpack

const (
	Int IntType = 1 << iota
	Int8
	Int16
	Int32
	Int64
	UInt
	UInt8
	UInt16
	UInt32
	UInt64
	Float32
	Float64
)

// IntType defines a code alias for supported integer types.
type IntType uint16

// Is checks whether v bits is set in t bits.
func (t IntType) Is(v IntType) bool {
	return t&v == v
}
