package msgpack_test

import (
	"bytes"
	"math"
	"strings"
	"testing"
	"time"
	"unsafe"

	"github.com/stretchr/testify/require"

	"gitlab.com/komex/msgpack/v2"
)

func TestEncoder_Integer(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		typ       msgpack.IntType
		extractor func(v uint64, e *msgpack.Encoder) error
	}{
		{
			"Uint8",
			UInt8,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Uint8(uint8(v))
			},
		},
		{
			"Uint16",
			UInt16,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Uint16(uint16(v))
			},
		},
		{
			"Uint32",
			UInt32,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Uint32(uint32(v))
			},
		},
		{
			"Uint64",
			UInt64,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Uint64(v)
			},
		},
		{
			"Uint",
			UInt,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Uint(uint(v))
			},
		},
		{
			"Int8",
			Int8,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Int8(int8(v))
			},
		},
		{
			"Int16",
			Int16,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Int16(int16(v))
			},
		},
		{
			"Int32",
			Int32,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Int32(int32(v))
			},
		},
		{
			"Int64",
			Int64,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Int64(int64(v))
			},
		},
		{
			"Int",
			Int,
			func(v uint64, e *msgpack.Encoder) error {
				return e.Int(int(v))
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			for _, def := range NewDataDefList(test.typ) {
				t.Run(def.Name, func(t *testing.T) {
					t.Parallel()

					var dv uint64
					switch v := def.Value.(type) {
					case int:
						dv = uint64(v)
					case uint64:
						dv = v
					default:
						t.Fatalf("Fix test: unexpected type of integer: %T", v)
					}

					var b bytes.Buffer
					e := msgpack.NewEncoder(&b)
					err := test.extractor(dv, e)

					if def.Methods.Is(test.typ) {
						require.NoError(t, err)
						require.Equal(t, def.Buf, b.Bytes())
					} else {
						require.ErrorIs(t, err, msgpack.ErrOverflow)
					}
				})
			}
		})
	}
}

func TestEncoder_ArrayLen(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		length  int
		buf     []byte
		wantErr bool
	}{
		{
			"fix",
			3,
			[]byte{0x93},
			false,
		},
		{
			"fix edge",
			15,
			[]byte{0x9f},
			false,
		},
		{
			"array 16",
			16,
			[]byte{0xdc, 0x00, 0x10},
			false,
		},
		{
			"array 16 edge",
			math.MaxUint16,
			[]byte{0xdc, 0xff, 0xff},
			false,
		},
		{
			"array 32",
			math.MaxUint16 + 1,
			[]byte{0xdd, 0x00, 0x01, 0x00, 0x00},
			false,
		},
		{
			"array 32 edge",
			math.MaxUint32,
			[]byte{0xdd, 0xff, 0xff, 0xff, 0xff},
			false,
		},
		{
			"overflows",
			math.MaxUint32 + 1,
			nil,
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			e := msgpack.NewEncoder(&b)
			err := e.ArrayLen(test.length)

			if (err == nil) == test.wantErr {
				t.Fatalf("unexpected error = %v", err)
			}

			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_MapLen(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name    string
		length  int
		buf     []byte
		wantErr bool
	}{
		{
			"fix",
			3,
			[]byte{0x83},
			false,
		},
		{
			"fix edge",
			15,
			[]byte{0x8f},
			false,
		},
		{
			"map 16",
			16,
			[]byte{0xde, 0x00, 0x10},
			false,
		},
		{
			"map 16 edge",
			math.MaxUint16,
			[]byte{0xde, 0xff, 0xff},
			false,
		},
		{
			"map 32",
			math.MaxUint16 + 1,
			[]byte{0xdf, 0x00, 0x01, 0x00, 0x00},
			false,
		},
		{
			"map 32 edge",
			math.MaxUint32,
			[]byte{0xdf, 0xff, 0xff, 0xff, 0xff},
			false,
		},
		{
			"overflows",
			math.MaxUint32 + 1,
			nil,
			true,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			e := msgpack.NewEncoder(&b)
			err := e.MapLen(test.length)

			if (err == nil) == test.wantErr {
				t.Fatalf("unexpected error = %v", err)
			}

			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Reset(t *testing.T) {
	t.Parallel()

	var (
		b1 bytes.Buffer
		b2 bytes.Buffer
	)

	e := msgpack.NewEncoder(&b1)

	require.NoError(t, e.Uint8(3))
	require.Equal(t, 1, b1.Len())
	require.Equal(t, 0, b2.Len())

	e.Reset(&b2)

	require.NoError(t, e.Uint16(math.MaxUint16))
	require.Equal(t, 1, b1.Len())
	require.Equal(t, 3, b2.Len())
}

func TestEncoder_Float32(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name  string
		value float32
		buf   []byte
	}{
		{
			"zero",
			0,
			[]byte{0xca, 0x00, 0x00, 0x00, 0x00},
		},
		{
			"min float32",
			-math.MaxFloat32,
			[]byte{0xca, 0xff, 0x7f, 0xff, 0xff},
		},
		{
			"max float32",
			math.MaxFloat32,
			[]byte{0xca, 0x7f, 0x7f, 0xff, 0xff},
		},
		{
			"64 to 32",
			float32(2.718281828459045),
			[]byte{0xca, 0x40, 0x2d, 0xf8, 0x54},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			enc := msgpack.NewEncoder(&b)

			require.NoError(t, enc.Float32(test.value))
			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Float64(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name  string
		value float64
		buf   []byte
	}{
		{
			"zero",
			0,
			[]byte{0xcb, 0x00, 0x00, 0x00, 0x00, 0x0, 0x0, 0x0, 0x0},
		},
		{
			"negative float64",
			-math.MaxFloat64,
			[]byte{0xcb, 0xff, 0xef, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		},
		{
			"positive float64",
			math.MaxFloat64,
			[]byte{0xcb, 0x7f, 0xef, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
		},
		{
			"64 to 32",
			2.718281828459045,
			[]byte{0xcb, 0x40, 0x05, 0xbf, 0x0a, 0x8b, 0x14, 0x57, 0x69},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			enc := msgpack.NewEncoder(&b)

			require.NoError(t, enc.Float64(test.value))
			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Bool(t *testing.T) {
	t.Parallel()

	var b bytes.Buffer

	e := msgpack.NewEncoder(&b)

	require.NoError(t, e.Bool(true))
	require.NoError(t, e.Bool(false))
	require.Equal(t, []byte{0xc3, 0xc2}, b.Bytes())
}

func TestEncoder_Nil(t *testing.T) {
	t.Parallel()

	var b bytes.Buffer

	e := msgpack.NewEncoder(&b)

	require.NoError(t, e.Nil())
	require.Equal(t, []byte{0xc0}, b.Bytes())
}

func TestEncoder_String(t *testing.T) {
	t.Parallel()

	const maxFixStr = 31
	tests := []struct {
		name string
		v    string
		buf  []byte
	}{
		{
			"empty",
			"",
			[]byte{0xa0},
		},
		{
			"fixstr",
			strings.Repeat("a", maxFixStr),
			append([]byte{0xbf}, bytes.Repeat([]byte{'a'}, maxFixStr)...),
		},
		{
			"str8 min",
			strings.Repeat("b", maxFixStr+1),
			append([]byte{0xd9, maxFixStr + 1}, bytes.Repeat([]byte{'b'}, maxFixStr+1)...),
		},
		{
			"str8",
			strings.Repeat("c", math.MaxUint8),
			append([]byte{0xd9, math.MaxUint8}, bytes.Repeat([]byte{'c'}, math.MaxUint8)...),
		},
		{
			"str16",
			strings.Repeat("d", math.MaxUint16),
			append([]byte{0xda, 0xff, 0xff}, bytes.Repeat([]byte{'d'}, math.MaxUint16)...),
		},
		{
			"str32 min",
			strings.Repeat("f", math.MaxUint16+1),
			append([]byte{0xdb, 0x00, 0x01, 0x00, 0x00}, bytes.Repeat([]byte{'f'}, math.MaxUint16+1)...),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			e := msgpack.NewEncoder(&b)

			require.NoError(t, e.String(test.v))
			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Bytes(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		v    []byte
		buf  []byte
	}{
		{
			"empty",
			nil,
			[]byte{0xc4, 0x00},
		},
		{
			"bin8",
			bytes.Repeat([]byte{'a'}, math.MaxUint8),
			append([]byte{0xc4, math.MaxUint8}, bytes.Repeat([]byte{'a'}, math.MaxUint8)...),
		},
		{
			"bin16",
			bytes.Repeat([]byte{'b'}, math.MaxUint16),
			append([]byte{0xc5, 0xff, 0xff}, bytes.Repeat([]byte{'b'}, math.MaxUint16)...),
		},
		{
			"bin32 min",
			bytes.Repeat([]byte{'c'}, math.MaxUint16+1),
			append([]byte{0xc6, 0x00, 0x01, 0x00, 0x00}, bytes.Repeat([]byte{'c'}, math.MaxUint16+1)...),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer
			e := msgpack.NewEncoder(&b)

			require.NoError(t, e.Bytes(test.v))
			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Errors(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		fn   func(*msgpack.Encoder) error
		err  error
	}{
		{
			"string length too long",
			func(encoder *msgpack.Encoder) error {
				// Hack! Creates an empty string with required length for test.
				// This hack allows to safe a lot of memory for test.
				// We need to be very careful, and we must ensure that the data is not read.
				s := unsafe.String(unsafe.StringData("test"), math.MaxUint32+1)

				return encoder.String(s)
			},
			msgpack.ErrLengthTooLong,
		},
		{
			"array length too long",
			func(encoder *msgpack.Encoder) error {
				return encoder.ArrayLen(math.MaxUint32 + 1)
			},
			msgpack.ErrLengthTooLong,
		},
		{
			"map length too long",
			func(encoder *msgpack.Encoder) error {
				return encoder.MapLen(math.MaxUint32 + 1)
			},
			msgpack.ErrLengthTooLong,
		},
		{
			"negative array length",
			func(encoder *msgpack.Encoder) error {
				return encoder.ArrayLen(-1)
			},
			msgpack.ErrNegativeLength,
		},
		{
			"negative map length",
			func(encoder *msgpack.Encoder) error {
				return encoder.MapLen(-1)
			},
			msgpack.ErrNegativeLength,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			require.ErrorIs(t, test.fn(msgpack.NewEncoder(new(bytes.Buffer))), test.err)
		})
	}
}

func TestEncoder_Time(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		time time.Time
		buf  []byte
	}{
		{
			"timestamp 32",
			time.Date(2020, time.July, 27, 17, 12, 10, 0, time.UTC),
			[]byte{0xd6, 0xff, 0x5f, 0x1f, 0x0a, 0xea},
		},
		{
			"timestamp 64",
			time.Date(2020, time.July, 27, 17, 12, 10, 571700177, time.UTC),
			[]byte{0xd7, 0xff, 0x88, 0x4d, 0xcf, 0x44, 0x5f, 0x1f, 0x0a, 0xea},
		},
		{
			"timestamp 96",
			time.Date(1595869930, time.July, 27, 17, 12, 10, 571700177, time.UTC),
			[]byte{0xc7, 0x0c, 0xff, 0x22, 0x13, 0x73, 0xd1, 0x00, 0xb2, 0xea, 0xd0, 0xdc, 0x9e, 0xa6, 0x6a},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer

			require.NoError(t, msgpack.NewEncoder(&b).Time(test.time))
			require.Equal(t, test.buf, b.Bytes())
		})
	}
}

func TestEncoder_Interface(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		data any
		want any
	}{
		{
			"int",
			-456,
			int16(-456),
		},
		{
			"bool",
			true,
			true,
		},
		{
			"array",
			[...]string{"dd", "ff"},
			[]any{"dd", "ff"},
		},
		{
			"slice",
			[]string{"dd", "ff"},
			[]any{"dd", "ff"},
		},
		{
			"map",
			map[string]int16{"dd": -1, "ff": 3456},
			map[any]any{"dd": int8(-1), "ff": uint16(3456)},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			var b bytes.Buffer

			e := msgpack.NewEncoder(&b)
			require.NoError(t, e.Interface(test.data))

			d := msgpack.NewDecoder(&b)
			v, err := d.Interface()
			require.NoError(t, err)
			require.Equal(t, test.want, v)
		})
	}
}
