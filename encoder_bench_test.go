package msgpack_test

import (
	"strings"
	"testing"
	"time"

	"gitlab.com/komex/msgpack/v2"
)

type noopWriter struct{}

func (noopWriter) WriteByte(byte) error {
	return nil
}

func (noopWriter) Write(p []byte) (int, error) {
	return len(p), nil
}

func BenchmarkEncoder_Integer(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	benchmarks := []struct {
		name string
		fn   func(v int) error
	}{
		{
			"Uint8",
			func(v int) error {
				return e.Uint8(uint8(v))
			},
		},
		{
			"Uint16",
			func(v int) error {
				return e.Uint16(uint16(v))
			},
		},
		{
			"Uint32",
			func(v int) error {
				return e.Uint32(uint32(v))
			},
		},
		{
			"Uint64",
			func(v int) error {
				return e.Uint64(uint64(v))
			},
		},
		{
			"Int8",
			func(v int) error {
				return e.Int8(int8(v))
			},
		},
		{
			"Int16",
			func(v int) error {
				return e.Int16(int16(v))
			},
		},
		{
			"Int32",
			func(v int) error {
				return e.Int32(int32(v))
			},
		},
		{
			"Int64",
			func(v int) error {
				return e.Int64(int64(v))
			},
		},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()

			for i := 0; i < b.N; i++ {
				if err := bm.fn(i); err != nil {
					b.Fatal(err)
				}
			}
		})
	}
}

func BenchmarkEncoder_Interface(b *testing.B) {
	integer := -300
	benchmarks := []struct {
		name  string
		value any
	}{
		{
			"uint8",
			uint8(200),
		},
		{
			"int",
			integer,
		},
		{
			"*int",
			&integer,
		},
		{
			"float",
			3.14,
		},
		{
			"string",
			"test value",
		},
		{
			"array",
			[...]int{3, 4, 5, -1},
		},
		{
			"slice",
			[]int{3, 4, 5, -1},
		},
		{
			"map",
			map[string]int{"a": 3, "b": 4, "c": 5, "d": -1},
		},
	}

	for _, bm := range benchmarks {
		b.Run(bm.name, func(b *testing.B) {
			b.ReportAllocs()

			e := msgpack.NewEncoder(new(noopWriter))

			for i := 0; i < b.N; i++ {
				if err := e.Interface(bm.value); err != nil {
					b.Fatal(err)
				}
			}
		})
	}
}

func BenchmarkEncoder_ArrayLen(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		if err := e.ArrayLen(i); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_MapLen(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		if err := e.MapLen(i); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_Bool(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		if err := e.Bool(i%2 == 0); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_Float32(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		v := float32(i) / 10000
		if err := e.Float32(v); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_Float64(b *testing.B) {
	b.ReportAllocs()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		v := float64(i) / 10000
		if err := e.Float64(v); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_Time(b *testing.B) {
	e := msgpack.NewEncoder(new(noopWriter))
	v := time.Now()

	b.ReportAllocs()

	for i := 0; i < b.N; i++ {
		if err := e.Time(v); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkEncoder_String(b *testing.B) {
	smallPayload := strings.Repeat("t", int(msgpack.FormatFixStringMax-msgpack.FormatFixStringMin)-2)
	bigPayload := strings.Repeat("t", int(msgpack.FormatFixStringMax))

	b.ReportAllocs()
	b.ResetTimer()

	e := msgpack.NewEncoder(new(noopWriter))

	for i := 0; i < b.N; i++ {
		p := smallPayload
		if i%2 == 0 {
			p = bigPayload
		}

		if err := e.String(p); err != nil {
			b.Fatal(err)
		}
	}
}
