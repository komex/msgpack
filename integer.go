package msgpack

// Integer defines interface for all types of integer.
type Integer interface {
	int8 | int16 | int32 | int64 | uint8 | uint16 | uint32 | uint64
}
