package msgpack_test

import (
	"bytes"
	"errors"
	"io"
	"math"
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/komex/msgpack/v2"
)

func TestDecoder_Integer(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name      string
		typ       msgpack.IntType
		extractor func(*msgpack.Decoder) (any, error)
	}{
		{
			"Uint8",
			UInt8,
			func(d *msgpack.Decoder) (any, error) {
				return d.Uint8()
			},
		},
		{
			"Uint16",
			UInt16,
			func(d *msgpack.Decoder) (any, error) {
				return d.Uint16()
			},
		},
		{
			"Uint32",
			UInt32,
			func(d *msgpack.Decoder) (any, error) {
				return d.Uint32()
			},
		},
		{
			"Uint64",
			UInt64,
			func(d *msgpack.Decoder) (any, error) {
				return d.Uint64()
			},
		},
		{
			"Uint",
			UInt,
			func(d *msgpack.Decoder) (any, error) {
				return d.Uint()
			},
		},
		{
			"Int8",
			Int8,
			func(d *msgpack.Decoder) (any, error) {
				return d.Int8()
			},
		},
		{
			"Int16",
			Int16,
			func(d *msgpack.Decoder) (any, error) {
				return d.Int16()
			},
		},
		{
			"Int32",
			Int32,
			func(d *msgpack.Decoder) (any, error) {
				return d.Int32()
			},
		},
		{
			"Int64",
			Int64,
			func(d *msgpack.Decoder) (any, error) {
				return d.Int64()
			},
		},
		{
			"Int",
			Int,
			func(d *msgpack.Decoder) (any, error) {
				return d.Int()
			},
		},
		{
			"Float32",
			Float32,
			func(d *msgpack.Decoder) (any, error) {
				return d.Float32()
			},
		},
		{
			"Float64",
			Float64,
			func(d *msgpack.Decoder) (any, error) {
				return d.Float64()
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			for _, def := range NewDataDefList(test.typ) {
				t.Run(def.Name, func(t *testing.T) {
					t.Parallel()

					d := msgpack.NewDecoder(bytes.NewReader(def.Buf))
					v, err := test.extractor(d)

					require.NoError(t, err)

					switch v.(type) {
					case float32:
						require.InEpsilon(t, def.Value, v, 0.0000001)
					case float64:
						require.InEpsilon(t, def.Value, v, 0.000000000000001)
					default:
						require.EqualValues(t, def.Value, v)
					}
				})
			}
		})
	}
}

func TestDecoder_Float32(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		buf  []byte
	}{
		{
			"float32",
			[]byte{0xca, 0x40, 0x48, 0xf5, 0xc3},
		},
		{
			"float64",
			[]byte{0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			v, err := d.Float32()
			require.NoError(t, err)
			require.InEpsilon(t, float32(3.14), v, 0.01)
		})
	}
}

func TestDecoder_Float64(t *testing.T) {
	t.Parallel()

	const (
		expected = 3.14
		accuracy = 0.000001
	)

	tests := []struct {
		name string
		buf  []byte
	}{
		{
			"float32",
			[]byte{0xca, 0x40, 0x48, 0xf5, 0xc3},
		},
		{
			"float64",
			[]byte{0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			v, err := d.Float64()
			require.NoError(t, err)
			require.LessOrEqual(t, math.Abs(v-expected), accuracy)
		})
	}
}

func TestDecoder_String(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected string
	}{
		{
			"fixstr empty",
			[]byte{0xa0},
			"",
		},
		{
			"fixstr",
			[]byte{0xa1, 0x61},
			"a",
		},
		{
			"str8",
			[]byte{0xd9, 0x02, 0x62, 0x63},
			"bc",
		},
		{
			"str16",
			[]byte{0xda, 0x00, 0x03, 0x64, 0x65, 0x66},
			"def",
		},
		{
			"str32",
			[]byte{0xdb, 0x00, 0x00, 0x00, 0x04, 0x67, 0x68, 0x69, 0x70},
			"ghip",
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			v, err := d.String()
			require.NoError(t, err)
			require.Equal(t, test.expected, v)
		})
	}
}

func TestDecoder_Bytes(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected []byte
	}{
		{
			"bin8",
			[]byte{0xc4, 0x02, 0x62, 0x63},
			[]byte{0x62, 0x63},
		},
		{
			"bin16",
			[]byte{0xc5, 0x00, 0x03, 0x64, 0x65, 0x66},
			[]byte{0x64, 0x65, 0x66},
		},
		{
			"bin32",
			[]byte{0xc6, 0x00, 0x00, 0x00, 0x04, 0x67, 0x68, 0x69, 0x70},
			[]byte{0x67, 0x68, 0x69, 0x70},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			v, err := d.Bytes()
			require.NoError(t, err)
			require.Equal(t, test.expected, v)
		})
	}
}

func TestDecoder_Bool(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected bool
	}{
		{"false", []byte{0xc2}, false},
		{"true", []byte{0xc3}, true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))
			v, err := d.Bool()

			require.NoError(t, err)
			require.Equal(t, test.expected, v)
		})
	}
}

func TestDecoder_Nil(t *testing.T) {
	t.Parallel()

	buf := []byte{0xc0}
	d := msgpack.NewDecoder(bytes.NewReader(buf))

	require.NoError(t, d.Nil())
}

func TestDecoder_Time(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected time.Time
	}{
		{
			"timestamp 32",
			[]byte{0xd6, 0xff, 0x5f, 0x1f, 0x0a, 0xea},
			time.Date(2020, time.July, 27, 17, 12, 10, 0, time.UTC),
		},
		{
			"timestamp 64",
			[]byte{0xd7, 0xff, 0x88, 0x4d, 0xcf, 0x44, 0x5f, 0x1f, 0x0a, 0xea},
			time.Date(2020, time.July, 27, 17, 12, 10, 571700177, time.UTC),
		},
		{
			"timestamp 96",
			[]byte{0xc7, 0x0c, 0xff, 0x22, 0x13, 0x73, 0xd1, 0x00, 0xb2, 0xea, 0xd0, 0xdc, 0x9e, 0xa6, 0x6a},
			time.Date(1595869930, time.July, 27, 17, 12, 10, 571700177, time.UTC),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			v, err := d.Time()
			require.NoError(t, err)
			require.Equal(t, test.expected, v)
		})
	}
}

func TestDecoder_Skip(t *testing.T) {
	t.Parallel()

	buf := []byte{
		0x82, 0xa4, 0x62, 0x6f, 0x6f, 0x6c, 0xc3, 0xa3, 0x6d,
		0x61, 0x70, 0x82, 0xa5, 0x61, 0x72, 0x72, 0x61, 0x79,
		0x93, 0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85,
		0x1f, 0xa2, 0x64, 0x64, 0xc0, 0xa6, 0x73, 0x74, 0x72,
		0x69, 0x6e, 0x67, 0xa3, 0x61, 0x62, 0x63,
		0xd6, 0xff, 0x5f, 0x1f, 0x0a, 0xea,
	}
	d := msgpack.NewDecoder(bytes.NewReader(buf))

	for {
		err := d.Skip()
		if errors.Is(err, io.EOF) {
			break
		}

		require.NoError(t, err)
	}
}

func TestDecoder_Interface(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		buf  []byte
		want any
	}{
		{
			"null",
			[]byte{0xc0},
			nil,
		},
		{
			"true",
			[]byte{0xc3},
			true,
		},
		{
			"false",
			[]byte{0xc2},
			false,
		},
		{
			"small positive int",
			[]byte{0x01},
			uint8(1),
		},
		{
			"positive int8",
			[]byte{0xcc, 0xc8},
			uint8(200),
		},
		{
			"positive int16",
			[]byte{0xcd, 0x01, 0x2c},
			uint16(300),
		},
		{
			"positive int32",
			[]byte{0xce, 0x00, 0x01, 0x01, 0xd0},
			uint32(66000),
		},
		{
			"positive int64",
			[]byte{0xcf, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00},
			uint64(4294967296),
		},

		{
			"small negative int",
			[]byte{0xff},
			int8(-1),
		},
		{
			"negative int8",
			[]byte{0xd0, 0x81},
			int8(-127),
		},
		{
			"negative int16",
			[]byte{0xd1, 0xff, 0x80},
			int16(-128),
		},
		{
			"negative int32",
			[]byte{0xd2, 0xff, 0xff, 0x7f, 0xff},
			int32(-32769),
		},
		{
			"negative int64",
			[]byte{0xd3, 0xff, 0xff, 0xff, 0xff, 0x7f, 0xff, 0xff, 0xff},
			int64(-2147483649),
		},

		{
			"float",
			[]byte{0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
			3.14,
		},

		{
			"small string",
			[]byte{0xa1, 0x61},
			"a",
		},
		{
			"bin8",
			[]byte{0xc4, 0x01, 0x61},
			[]byte("a"),
		},

		{
			"array",
			[]byte{0x95, 0xa1, 0x61, 0x01, 0xc3, 0xc0, 0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f},
			[]any{"a", uint8(1), true, nil, 3.14},
		},
		{
			"map",
			[]byte{0x83, 0xa1, 0x61, 0x01, 0xc3, 0xc0, 0xcb, 0x40, 0x09, 0x1e, 0xb8, 0x51, 0xeb, 0x85, 0x1f, 0xfc},
			map[any]any{"a": uint8(1), true: nil, 3.14: int8(-4)},
		},

		{
			"ext",
			[]byte{0xd6, 0xff, 0x5f, 0x3a, 0x8c, 0x70},
			time.Date(2020, time.August, 17, 13, 56, 0, 0, time.UTC),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))
			v, err := d.Interface()

			require.NoError(t, err)
			require.Equal(t, test.want, v)
		})
	}
}

func TestDecoder_ArrayLen(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected int
	}{
		{
			"zero",
			[]byte{0x90},
			0,
		},
		{
			"4",
			[]byte{0x94, 0x01, 0x02, 0x03, 0x04},
			4,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			l, err := d.ArrayLen()
			require.NoError(t, err)
			require.Equal(t, test.expected, l)
		})
	}
}

func TestDecoder_MapLen(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name     string
		buf      []byte
		expected int
	}{
		{
			"zero",
			[]byte{0x80},
			0,
		},
		{
			"1",
			[]byte{0x81, 0xa1, 0x61, 0x01},
			1,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			d := msgpack.NewDecoder(bytes.NewReader(test.buf))

			l, err := d.MapLen()
			require.NoError(t, err)
			require.Equal(t, test.expected, l)
		})
	}
}

func TestDecoder_PeekFormat(t *testing.T) {
	t.Parallel()

	buf := []byte{0xd0, 0xdf}
	d := msgpack.NewDecoder(bytes.NewReader(buf))

	// First check.
	format, err := d.PeekFormat()
	require.NoError(t, err)
	require.Equal(t, msgpack.FormatInt8, format)

	// Second check.
	format, err = d.PeekFormat()
	require.NoError(t, err)
	require.Equal(t, msgpack.FormatInt8, format)
}
