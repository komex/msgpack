// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

import (
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"math"
	"reflect"
	"time"
	"unsafe"
)

// Decoder allows to decode msgpack stream from Reader.
type Decoder struct {
	r           Reader
	buf         []byte
	extDecoders map[Ext]ExtDecoderFn
}

// NewDecoder returns a new instance of msgpack Decoder.
func NewDecoder(r Reader) *Decoder {
	d := Decoder{r: r, buf: make([]byte, bits64), extDecoders: make(map[Ext]ExtDecoderFn, 1)}

	d.extDecoders[ExtTimestamp] = func(data []byte) (any, error) {
		return d.extTime(data)
	}

	return &d
}

// Reset resets data source.
func (d *Decoder) Reset(r Reader) {
	d.r = r
}

// RegisterExtDecoder registers a custom decoder for extensions with specified code.
func (d *Decoder) RegisterExtDecoder(code Ext, fn ExtDecoderFn) {
	if fn == nil {
		delete(d.extDecoders, code)
	} else {
		d.extDecoders[code] = fn
	}
}

// Uint8 decodes msgpack integer as uint8.
func (d *Decoder) Uint8() (uint8, error) {
	return readInteger[uint8](d, UInt8)
}

// Uint16 decodes msgpack integer as uint16.
func (d *Decoder) Uint16() (uint16, error) {
	return readInteger[uint16](d, UInt16)
}

// Uint32 decodes msgpack integer as uint32.
func (d *Decoder) Uint32() (uint32, error) {
	return readInteger[uint32](d, UInt32)
}

// Uint64 decodes msgpack integer as uint64.
func (d *Decoder) Uint64() (uint64, error) {
	return readInteger[uint64](d, UInt64)
}

// Uint decodes msgpack integer as uint.
func (d *Decoder) Uint() (uint, error) {
	if unsafe.Sizeof(uint(0)) == bits64 {
		v, err := d.Uint64()
		if err != nil {
			return 0, fmt.Errorf("read integer 64: %w", err)
		}

		return uint(v), nil
	}

	v, err := d.Uint32()
	if err != nil {
		return 0, fmt.Errorf("read integer 32: %w", err)
	}

	return uint(v), nil
}

// Int8 decodes msgpack integer as int8.
func (d *Decoder) Int8() (int8, error) {
	return readInteger[int8](d, Int8)
}

// Int16 decodes msgpack integer as int16.
func (d *Decoder) Int16() (int16, error) {
	return readInteger[int16](d, Int16)
}

// Int32 decodes msgpack integer as int32.
func (d *Decoder) Int32() (int32, error) {
	return readInteger[int32](d, Int32)
}

// Int64 decodes msgpack integer as int64.
func (d *Decoder) Int64() (int64, error) {
	return readInteger[int64](d, Int64)
}

// Int decodes msgpack integer as int.
func (d *Decoder) Int() (int, error) {
	if unsafe.Sizeof(0) == bits64 {
		v, err := d.Int64()
		if err != nil {
			return 0, fmt.Errorf("read integer 64: %w", err)
		}

		return int(v), nil
	}

	v, err := d.Int32()
	if err != nil {
		return 0, fmt.Errorf("read integer 32: %w", err)
	}

	return int(v), nil
}

// MapLen decodes size of msgpack map.
func (d *Decoder) MapLen() (int, error) {
	format, err := d.Format()
	if err != nil {
		return 0, fmt.Errorf("fetch format: %w", err)
	}

	l, err := d.mapLength(format)
	if err != nil {
		return 0, fmt.Errorf("map length: %w", err)
	}

	return l, err
}

// ArrayLen decodes size of msgpack array.
func (d *Decoder) ArrayLen() (int, error) {
	format, err := d.Format()
	if err != nil {
		return 0, fmt.Errorf("fetch format: %w", err)
	}

	l, err := d.arrayLength(format)
	if err != nil {
		return 0, fmt.Errorf("array length: %w", err)
	}

	return l, nil
}

// Bytes decodes msgpack string or bin as a bytes slice.
func (d *Decoder) Bytes() ([]byte, error) {
	format, err := d.Format()
	if err != nil {
		return nil, fmt.Errorf("fetch format: %w", err)
	}

	data, err := d.bytes(format)
	if err != nil {
		return nil, fmt.Errorf("decode bytes: %w", err)
	}

	return data, nil
}

// String decodes msgpack string or bin as a string.
func (d *Decoder) String() (string, error) {
	format, err := d.Format()
	if err != nil {
		return "", fmt.Errorf("fetch format: %w", err)
	}

	data, err := d.string(format)
	if err != nil {
		return "", fmt.Errorf("decode string: %w", err)
	}

	return data, nil
}

// Float32 decodes msgpack float as float32.
func (d *Decoder) Float32() (float32, error) {
	v, err := d.Float64()
	if err != nil {
		return 0, err
	}

	return float32(v), nil
}

// Float64 decodes msgpack float as float64.
func (d *Decoder) Float64() (float64, error) {
	f, err := d.Format()
	if err != nil {
		return 0, fmt.Errorf("fetch format: %w", err)
	}

	var v float64

	if v, err = d.float(f); err != nil {
		return 0, fmt.Errorf("parse float: %w", err)
	}

	return v, nil
}

// Bool decodes msgpack boolean value.
func (d *Decoder) Bool() (bool, error) {
	f, err := d.Format()
	if err != nil {
		return false, fmt.Errorf("fetch format: %w", err)
	}

	switch f {
	case FormatTrue:
		return true, nil
	case FormatFalse:
		return false, nil
	default:
		return false, fmt.Errorf("not bool format: %w", NewFormatError(f, ErrUnexpectedFormat))
	}
}

// Nil decodes msgpack nil.
func (d *Decoder) Nil() error {
	f, err := d.Format()
	if err != nil {
		return fmt.Errorf("fetch format: %w", err)
	}

	if f != FormatNil {
		return fmt.Errorf("not nil format: %w", NewFormatError(f, ErrUnexpectedFormat))
	}

	return nil
}

// Format returns current msgpack format code.
func (d *Decoder) Format() (Format, error) {
	b, err := d.r.ReadByte()
	if err != nil {
		return FormatUnknown, fmt.Errorf("read byte: %w", err)
	}

	return Format(b), nil
}

// PeekFormat returns current msgpack format code but next call Format will return the same format.
func (d *Decoder) PeekFormat() (Format, error) {
	f, err := d.Format()
	if err != nil {
		return FormatUnknown, fmt.Errorf("fetch format: %w", err)
	}

	if err = d.r.UnreadByte(); err != nil {
		return FormatUnknown, fmt.Errorf("unread peeked byte: %w", err)
	}

	return f, nil
}

// Skip skips current msgpack object.
func (d *Decoder) Skip() error {
	format, err := d.Format()
	if err != nil {
		return fmt.Errorf("fetch format: %w", err)
	}

	if format.IsFixedNum() {
		return nil
	}

	var length int

	length, err = d.fetchLength(format)
	if err != nil {
		return fmt.Errorf("fetch length: %w", err)
	}

	if format.IsExt() {
		// Increment length of package by 1 byte for skipping ext Type.
		length++
	}

	if length == 0 {
		return nil
	}

	switch {
	case format.IsArray():
		err = d.skipElements(length)
	case format.IsMap():
		// multiply defines multiply for map length. We need to skip `(key + value) * length` elements.
		const multiply = 2

		err = d.skipElements(length * multiply)
	default:
		err = d.discard(uint64(length))
	}

	if err != nil {
		return fmt.Errorf("skip data: %w", err)
	}

	return nil
}

// Interface decodes unknown type msgpack value.
func (d *Decoder) Interface() (any, error) {
	format, err := d.Format()
	if err != nil {
		return nil, fmt.Errorf("fetch format: %w", err)
	}

	switch format {
	case FormatNil:
		return nil, nil
	case FormatTrue:
		return true, nil
	case FormatFalse:
		return false, nil
	}

	var v any

	switch {
	case format.IsInteger():
		v, err = d.integer(format)
	case format.IsFloat():
		v, err = d.float(format)
	case format.IsBin():
		v, err = d.bytes(format)
	case format.IsString():
		v, err = d.string(format)
	case format.IsArray():
		v, err = d.decodeArray(format)
	case format.IsMap():
		v, err = d.decodeMap(format)
	case format.IsExt():
		if err = d.r.UnreadByte(); err != nil {
			return nil, fmt.Errorf("unread format byte: %w", err)
		}

		return d.Ext()
	}

	if err != nil {
		return nil, fmt.Errorf("decode interface value: %w", NewFormatError(format, err))
	}

	return v, nil
}

// Ext decodes extension with registered custom decoder.
func (d *Decoder) Ext() (any, error) {
	code, data, err := d.ExtData()
	if err != nil {
		return nil, fmt.Errorf("get ext data: %w", err)
	}

	if f, ok := d.extDecoders[code]; ok {
		return f(data)
	}

	return nil, fmt.Errorf("no registered decoder for type %s: %w", code, ErrUnsupportedType)
}

// Time decodes time from Timestamp extension.
func (d *Decoder) Time() (time.Time, error) {
	format, err := d.Format()
	if err != nil {
		return time.Time{}, fmt.Errorf("fetch format: %w", err)
	}

	var length int

	if length, err = d.extLength(format); err != nil {
		return time.Time{}, fmt.Errorf("read package length: %w", err)
	}

	// Check extension code.
	{
		var b byte

		if b, err = d.r.ReadByte(); err != nil {
			return time.Time{}, fmt.Errorf("read ext type: %w", err)
		}

		if ext := Ext(b); ext != ExtTimestamp {
			return time.Time{}, fmt.Errorf("not timestamp extension: %w", NewUnexpectedExtensionError(ext))
		}
	}

	var buf []byte
	if length <= cap(d.buf) {
		err = d.read(uint8(length))
		buf = d.buf[:length]
	} else {
		buf = make([]byte, length)
		_, err = io.ReadAtLeast(d.r, buf, length)
	}

	if err != nil {
		return time.Time{}, fmt.Errorf("read package data: %w", err)
	}

	return d.extTime(buf)
}

// ExtData decodes msgpack extension code and its data.
func (d *Decoder) ExtData() (Ext, []byte, error) {
	format, err := d.Format()
	if err != nil {
		return 0, nil, fmt.Errorf("fetch format: %w", err)
	}

	var l int

	if l, err = d.extLength(format); err != nil {
		return 0, nil, fmt.Errorf("read package length: %w", err)
	}

	var b byte

	if b, err = d.r.ReadByte(); err != nil {
		return 0, nil, fmt.Errorf("read ext type: %w", err)
	}

	if l == 0 {
		return Ext(b), nil, nil
	}

	buf := make([]byte, l)
	if _, err = io.ReadAtLeast(d.r, buf, l); err != nil {
		return 0, nil, fmt.Errorf("read package data: %w", err)
	}

	return Ext(b), buf, nil
}

func (d *Decoder) integer(format Format) (any, error) {
	switch format {
	case FormatUint8:
		return read8[uint8](d)
	case FormatInt8:
		return read8[int8](d)
	case FormatUint16:
		return read16[uint16](d)
	case FormatInt16:
		return read16[int16](d)
	case FormatUint32:
		return read32[uint32](d)
	case FormatInt32:
		return read32[int32](d)
	case FormatUint64:
		return read64[uint64](d)
	case FormatInt64:
		return read64[int64](d)
	default:
		if !format.IsFixedNum() {
			return 0, ErrUnexpectedFormat
		}

		if format.IsNegativeInteger() {
			return int8(format), nil
		}

		return uint8(format), nil
	}
}

func (d *Decoder) float(format Format) (float64, error) {
	switch format {
	case FormatFloat64:
		v, err := d.read64()
		if err != nil {
			return 0, fmt.Errorf("read64: %w", err)
		}

		return math.Float64frombits(v), nil
	case FormatFloat32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return float64(math.Float32frombits(v)), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) bytes(f Format) ([]byte, error) {
	l, err := d.binaryLength(f)
	if err != nil {
		return nil, fmt.Errorf("read bin length: %w", err)
	}

	if l == 0 {
		return nil, nil
	}

	buf := make([]byte, l)
	if _, err = io.ReadAtLeast(d.r, buf, l); err != nil {
		return nil, fmt.Errorf("read package data: %w", err)
	}

	return buf, nil
}

func (d *Decoder) string(format Format) (string, error) {
	l, err := d.stringLength(format)
	if err != nil {
		return "", fmt.Errorf("read string length: %w", err)
	}

	if l == 0 {
		return "", nil
	}

	buf := make([]byte, l)
	if _, err = io.ReadAtLeast(d.r, buf, l); err != nil {
		return "", fmt.Errorf("read package data: %w", err)
	}

	return *(*string)(unsafe.Pointer(&buf)), nil
}

func (d *Decoder) decodeArray(f Format) ([]any, error) {
	l, err := d.arrayLength(f)
	if err != nil {
		return nil, fmt.Errorf("decode array length: %w", err)
	}

	out := make([]any, l)
	for i := range out {
		if out[i], err = d.Interface(); err != nil {
			return nil, fmt.Errorf("decode array element: %w", err)
		}
	}

	return out, nil
}

func (d *Decoder) decodeMap(f Format) (any, error) {
	l, err := d.mapLength(f)
	if err != nil {
		return nil, fmt.Errorf("decode map length: %w", err)
	}

	out := make(map[any]any, l)

	var k any

	for ; l > 0; l-- {
		k, err = d.Interface()
		if err == nil {
			t := reflect.TypeOf(k)
			if t == nil || !t.Comparable() {
				err = ErrUnsupportedMapKey
			}
		}

		if err != nil {
			return nil, fmt.Errorf("decode map key: %w", err)
		}

		if out[k], err = d.Interface(); err != nil {
			return nil, fmt.Errorf("decode map value: %w", err)
		}
	}

	return out, nil
}

func (d *Decoder) arrayLength(format Format) (int, error) {
	if format >= FormatFixArrayMin && format <= FormatFixArrayMax {
		return int(format - FormatFixArrayMin), nil
	}

	switch format {
	case FormatArray16:
		v, err := d.read16()
		if err != nil {
			return 0, fmt.Errorf("read16: %w", err)
		}

		return int(v), nil
	case FormatArray32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return int(v), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) mapLength(format Format) (int, error) {
	if format >= FormatFixMapMin && format <= FormatFixMapMax {
		return int(format - FormatFixMapMin), nil
	}

	switch format {
	case FormatMap16:
		v, err := d.read16()
		if err != nil {
			return 0, fmt.Errorf("read16: %w", err)
		}

		return int(v), nil
	case FormatMap32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return int(v), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) extLength(format Format) (int, error) {
	switch format {
	case FormatFixExt1:
		return bits8, nil
	case FormatFixExt2:
		return bits16, nil
	case FormatFixExt4:
		return bits32, nil
	case FormatFixExt8:
		return bits64, nil
	case FormatFixExt16:
		return bits128, nil
	case FormatExt8:
		v, err := d.r.ReadByte()
		if err != nil {
			return 0, fmt.Errorf("read8: %w", err)
		}

		return int(v), nil
	case FormatExt16:
		v, err := d.read16()
		if err != nil {
			return 0, fmt.Errorf("read16: %w", err)
		}

		return int(v), nil
	case FormatExt32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return int(v), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) stringLength(format Format) (int, error) {
	if format >= FormatFixStringMin && format <= FormatFixStringMax {
		return int(format - FormatFixStringMin), nil
	}

	switch format {
	case FormatString8:
		v, err := d.r.ReadByte()
		if err != nil {
			return 0, fmt.Errorf("read8: %w", err)
		}

		return int(v), nil
	case FormatString16:
		v, err := d.read16()
		if err != nil {
			return 0, fmt.Errorf("read16: %w", err)
		}

		return int(v), nil
	case FormatString32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return int(v), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) binaryLength(format Format) (int, error) {
	switch format {
	case FormatBin8:
		v, err := d.r.ReadByte()
		if err != nil {
			return 0, fmt.Errorf("read8: %w", err)
		}

		return int(v), nil
	case FormatBin16:
		v, err := d.read16()
		if err != nil {
			return 0, fmt.Errorf("read16: %w", err)
		}

		return int(v), nil
	case FormatBin32:
		v, err := d.read32()
		if err != nil {
			return 0, fmt.Errorf("read32: %w", err)
		}

		return int(v), nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (d *Decoder) discard(n uint64) error {
	if _, err := io.CopyN(io.Discard, d.r, int64(n)); err != nil {
		return fmt.Errorf("discard bytes: %w", err)
	}

	return nil
}

func (d *Decoder) read(bytes uint8) error {
	// Reduce the buffer size to the required value.
	d.buf = d.buf[:bytes]
	if _, err := io.ReadAtLeast(d.r, d.buf, len(d.buf)); err != nil {
		return fmt.Errorf("read package data: %w", err)
	}

	return nil
}

func (d *Decoder) read16() (uint16, error) {
	if err := d.read(bits16); err != nil {
		return 0, err
	}

	return binary.BigEndian.Uint16(d.buf), nil
}

func (d *Decoder) read32() (uint32, error) {
	if err := d.read(bits32); err != nil {
		return 0, err
	}

	return binary.BigEndian.Uint32(d.buf), nil
}

func (d *Decoder) read64() (uint64, error) {
	if err := d.read(bits64); err != nil {
		return 0, err
	}

	return binary.BigEndian.Uint64(d.buf), nil
}

func (d *Decoder) skipElements(n int) error {
	for ; n > 0; n-- {
		if err := d.Skip(); err != nil {
			return fmt.Errorf("skip array element: %w", err)
		}
	}

	return nil
}

func (d *Decoder) fetchLength(format Format) (int, error) {
	checks := [...]func(Format) (int, error){
		d.fixedLength,
		d.binaryLength,
		d.stringLength,
		d.arrayLength,
		d.mapLength,
		d.extLength,
	}

	var (
		length int
		err    error
	)

	for _, check := range checks {
		length, err = check(format)
		if !errors.Is(err, ErrUnexpectedFormat) {
			break
		}
	}

	if err != nil {
		return 0, fmt.Errorf("check data length: %w", err)
	}

	return length, nil
}

func (*Decoder) fixedLength(format Format) (int, error) {
	switch format {
	case FormatUint8, FormatInt8:
		return bits8, nil
	case FormatUint16, FormatInt16:
		return bits16, nil
	case FormatUint32, FormatInt32, FormatFloat32:
		return bits32, nil
	case FormatUint64, FormatInt64, FormatFloat64:
		return bits64, nil
	case FormatNil, FormatTrue, FormatFalse:
		return 0, nil
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}
}

func (*Decoder) extTime(data []byte) (time.Time, error) {
	var (
		sec     int64
		nanoSec int64
	)

	switch len(data) {
	case bits32:
		sec = int64(binary.BigEndian.Uint32(data))
	case bits32 + bits64:
		nanoSec = int64(binary.BigEndian.Uint32(data[:bits32]))
		sec = int64(binary.BigEndian.Uint64(data[bits32:]))
	case bits64:
		const (
			maxNSec = 999999999
			secBits = 34
			secMask = 0x00000003ffffffff
		)

		u := binary.BigEndian.Uint64(data)
		nanoSec = int64(u >> secBits)

		if nanoSec > maxNSec {
			return time.Time{}, ErrNanosecondsTooLarge
		}

		sec = int64(u & secMask)
	default:
		return time.Time{}, ErrUnexpectedLength
	}

	return time.Unix(sec, nanoSec).UTC(), nil
}

func readInteger[T Integer](d *Decoder, typ IntType) (T, error) {
	format, err := d.Format()
	if err != nil {
		return 0, fmt.Errorf("fetch format: %w", err)
	}

	var (
		value    T
		supports IntType
	)

	switch {
	case format <= FormatPositiveFixIntMax:
		value = T(format)
		supports = UInt8 | UInt16 | UInt32 | UInt64 | Int8 | Int16 | Int32 | Int64
	case format >= FormatNegativeFixIntMin:
		value = T(int8(format))
		supports = Int8 | Int16 | Int32 | Int64
	case format >= FormatUint8 && format <= FormatUint64:
		value, supports, err = readUnsignedInteger[T](d, format)
	case format >= FormatInt8 && format <= FormatInt64:
		value, supports, err = readSignedInteger[T](d, format)
	default:
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}

	if err != nil {
		return 0, fmt.Errorf("decode value: %w", err)
	}

	if !supports.Is(typ) {
		return 0, NewFormatError(format, ErrUnexpectedFormat)
	}

	return value, nil
}

func readUnsignedInteger[T Integer](d *Decoder, format Format) (T, IntType, error) {
	var (
		value uint64
		err   error
	)

	switch format {
	case FormatUint8:
		err = d.read(bits8)
		value = uint64(d.buf[0])
	case FormatUint16:
		var rv uint16
		rv, err = d.read16()
		value = uint64(rv)
	case FormatUint32:
		var rv uint32
		rv, err = d.read32()
		value = uint64(rv)
	case FormatUint64:
		value, err = d.read64()
	default:
		return 0, 0, ErrUnexpectedFormat
	}

	if err != nil {
		return 0, 0, fmt.Errorf("read value: %w", err)
	}

	return T(value), unsignedIntegerSupports(value), nil
}

func readSignedInteger[T Integer](d *Decoder, format Format) (T, IntType, error) {
	var (
		value int64
		err   error
	)

	switch format {
	case FormatInt8:
		err = d.read(bits8)
		value = int64(int8(d.buf[0]))
	case FormatInt16:
		var rv uint16
		rv, err = d.read16()
		value = int64(int16(rv))
	case FormatInt32:
		var rv uint32
		rv, err = d.read32()
		value = int64(int32(rv))
	case FormatInt64:
		var rv uint64
		rv, err = d.read64()
		value = int64(rv)
	default:
		return 0, 0, ErrUnexpectedFormat
	}

	if err != nil {
		return 0, 0, fmt.Errorf("read value: %w", err)
	}

	return T(value), signedIntegerSupports(value), nil
}

func unsignedIntegerSupports(value uint64) IntType {
	supports := UInt8 | UInt16 | UInt32 | UInt64 | Int8 | Int16 | Int32 | Int64

	switch {
	case value > math.MaxInt64:
		supports ^= Int8 | UInt8 | Int16 | UInt16 | Int32 | UInt32 | Int64
	case value > math.MaxUint32:
		supports ^= Int8 | UInt8 | Int16 | UInt16 | Int32 | UInt32
	case value > math.MaxInt32:
		supports ^= Int8 | UInt8 | Int16 | UInt16 | Int32
	case value > math.MaxUint16:
		supports ^= Int8 | UInt8 | Int16 | UInt16
	case value > math.MaxInt16:
		supports ^= Int8 | UInt8 | Int16
	case value > math.MaxUint8:
		supports ^= Int8 | UInt8
	case value > math.MaxInt8:
		supports ^= Int8
	}

	return supports
}

func signedIntegerSupports(value int64) IntType {
	supports := Int8 | Int16 | Int32 | Int64

	switch {
	case value < math.MinInt32 || value > math.MaxInt32:
		supports ^= Int8 | Int16 | Int32
	case value < math.MinInt16 || value > math.MaxInt16:
		supports ^= Int8 | Int16
	case value < math.MinInt8 || value > math.MaxInt8:
		supports ^= Int8
	}

	if value >= 0 {
		supports |= UInt8 | UInt16 | UInt32 | UInt64

		switch {
		case value > math.MaxUint32:
			supports ^= UInt8 | UInt16 | UInt32
		case value > math.MaxUint16:
			supports ^= UInt8 | UInt16
		case value > math.MaxUint8:
			supports ^= UInt8
		}
	}

	return supports
}
