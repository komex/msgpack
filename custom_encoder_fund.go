package msgpack

// CustomEncoderFunc defines a function implemented CustomEncoder interface.
type CustomEncoderFunc func(*Encoder) error

// EncodeMsgpack implements CustomEncoder interface.
func (fn CustomEncoderFunc) EncodeMsgpack(enc *Encoder) error {
	return fn(enc)
}
