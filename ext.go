// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

//go:generate stringer -type=Ext -linecomment

const (
	// ExtTimestamp is a code of Timestamp extension.
	ExtTimestamp Ext = -1
)

// Ext represents codes of msgpack extension.
type Ext int8
