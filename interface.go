// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

type (
	// CustomDecoder declares method which indicates structure is support custom decoding from msgpack.
	CustomDecoder interface {
		DecodeMsgpack(dec *Decoder) error
	}

	// CustomEncoder declares method which indicates structure is support custom encoding from msgpack.
	CustomEncoder interface {
		EncodeMsgpack(enc *Encoder) error
	}
)
