# MessagePack for Golang

[![pipeline status](https://gitlab.com/komex/msgpack/badges/master/pipeline.svg)](https://gitlab.com/komex/msgpack/-/commits/master)
[![coverage report](https://gitlab.com/komex/msgpack/badges/master/coverage.svg)](https://gitlab.com/komex/msgpack/-/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/komex/msgpack)](https://goreportcard.com/report/gitlab.com/komex/msgpack)

## Installation
```sh
go get -u gitlab.com/komex/msgpack/v2
```

## License

This library is under the Apache License 2.0 License.
