package msgpack

import (
	"io"
)

// Writer describes interface required by Encoder.
type Writer interface {
	io.ByteWriter
	io.Writer
}
