package msgpack_test

import (
	"math"
	"slices"

	"gitlab.com/komex/msgpack/v2"
)

const (
	Int     = msgpack.Int
	Int8    = msgpack.Int8
	Int16   = msgpack.Int16
	Int32   = msgpack.Int32
	Int64   = msgpack.Int64
	UInt    = msgpack.UInt
	UInt8   = msgpack.UInt8
	UInt16  = msgpack.UInt16
	UInt32  = msgpack.UInt32
	UInt64  = msgpack.UInt64
	Float32 = msgpack.Float32
	Float64 = msgpack.Float64
)

type DataDef struct {
	Name    string
	Methods msgpack.IntType
	Buf     []byte
	Value   any
}

func NewDataDefList(filter msgpack.IntType) []DataDef {
	list := []DataDef{
		{
			Name:    "zero",
			Methods: Int | Int8 | Int16 | Int32 | Int64 | UInt | UInt8 | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0x00},
			Value:   0,
		},
		{
			Name:    "positive min",
			Methods: Int | Int8 | Int16 | Int32 | Int64 | UInt | UInt8 | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0x01},
			Value:   1,
		},
		{
			Name:    "positive max",
			Methods: Int | Int8 | Int16 | Int32 | Int64 | UInt | UInt8 | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0x7f},
			Value:   math.MaxInt8,
		},
		{
			Name:    "uint8 min",
			Methods: Int | Int16 | Int32 | Int64 | UInt | UInt8 | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0xcc, 0x80},
			Value:   math.MaxInt8 + 1,
		},
		{
			Name:    "uint8 max",
			Methods: Int | Int16 | Int32 | Int64 | UInt | UInt8 | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0xcc, 0xff},
			Value:   math.MaxUint8,
		},
		{
			Name:    "uint16 min",
			Methods: Int | Int16 | Int32 | Int64 | UInt | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0xcd, 0x01, 0x00},
			Value:   math.MaxUint8 + 1,
		},
		{
			Name:    "uint16 max",
			Methods: Int | Int32 | Int64 | UInt | UInt16 | UInt32 | UInt64,
			Buf:     []byte{0xcd, 0xff, 0xff},
			Value:   math.MaxUint16,
		},
		{
			Name:    "uint32 min",
			Methods: Int | Int32 | Int64 | UInt | UInt32 | UInt64,
			Buf:     []byte{0xce, 0x00, 0x01, 0x00, 0x00},
			Value:   math.MaxUint16 + 1,
		},
		{
			Name:    "uint32 max",
			Methods: Int | Int64 | UInt | UInt32 | UInt64,
			Buf:     []byte{0xce, 0xff, 0xff, 0xff, 0xff},
			Value:   math.MaxUint32,
		},
		{
			Name:    "uint64 min",
			Methods: Int | Int64 | UInt | UInt64,
			Buf:     []byte{0xcf, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00},
			Value:   math.MaxUint32 + 1,
		},
		{
			Name:    "uint64 max",
			Methods: UInt | UInt64,
			Buf:     []byte{0xcf, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
			Value:   uint64(math.MaxUint64),
		},
		{
			Name:    "negative max",
			Methods: Int | Int8 | Int16 | Int32 | Int64,
			Buf:     []byte{0xff},
			Value:   -1,
		},
		{
			Name:    "negative min",
			Methods: Int | Int8 | Int16 | Int32 | Int64,
			Buf:     []byte{0xe0},
			Value:   -32,
		},
		{
			Name:    "int8 max",
			Methods: Int | Int8 | Int16 | Int32 | Int64,
			Buf:     []byte{0xd0, 0xdf},
			Value:   -33,
		},
		{
			Name:    "int8 min",
			Methods: Int | Int8 | Int16 | Int32 | Int64,
			Buf:     []byte{0xd0, 0x80},
			Value:   math.MinInt8,
		},
		{
			Name:    "int16 max",
			Methods: Int | Int16 | Int32 | Int64,
			Buf:     []byte{0xd1, 0xff, 0x7f},
			Value:   math.MinInt8 - 1,
		},
		{
			Name:    "int16 min",
			Methods: Int | Int16 | Int32 | Int64,
			Buf:     []byte{0xd1, 0x80, 0x00},
			Value:   math.MinInt16,
		},
		{
			Name:    "int32 max",
			Methods: Int | Int32 | Int64,
			Buf:     []byte{0xd2, 0xff, 0xff, 0x7f, 0xff},
			Value:   math.MinInt16 - 1,
		},
		{
			Name:    "int32 min",
			Methods: Int | Int32 | Int64,
			Buf:     []byte{0xd2, 0x80, 0x00, 0x00, 0x00},
			Value:   math.MinInt32,
		},
		{
			Name:    "int64 max",
			Methods: Int | Int64,
			Buf:     []byte{0xd3, 0xff, 0xff, 0xff, 0xff, 0x7f, 0xff, 0xff, 0xff},
			Value:   math.MinInt32 - 1,
		},
		{
			Name:    "int64 min",
			Methods: Int | Int64,
			Buf:     []byte{0xd3, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
			Value:   math.MinInt64,
		},
		{
			Name:    "float32",
			Methods: Float32 | Float64,
			Buf:     []byte{0xca, 0x40, 0x2d, 0xf8, 0x54},
			Value:   float32(2.7182817),
		},
		{
			Name:    "float64",
			Methods: Float32 | Float64,
			Buf:     []byte{0xcb, 0x40, 0x05, 0xbf, 0x0a, 0x8b, 0x14, 0x57, 0x69},
			Value:   2.718281828459045,
		},
	}

	if filter == 0 {
		return list
	}

	list = slices.DeleteFunc(list, func(def DataDef) bool {
		return !def.Methods.Is(filter)
	})

	return list
}
