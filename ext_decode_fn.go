package msgpack

// ExtDecoderFn defines an interface for custom extension decoders.
type ExtDecoderFn func([]byte) (any, error)
