// Package msgpack implements functions for the encoding and decoding values in msgpack format.
package msgpack

//go:generate stringer -type=Format -trimprefix=Format

const (
	// FormatUnknown is default format code witch means a code is unknown.
	FormatUnknown Format = 0
)

// Codes for range formats.
const (
	FormatPositiveFixIntMax Format = 0x7f
	FormatNegativeFixIntMin Format = 0xe0

	FormatFixMapMin Format = 0x80
	FormatFixMapMax Format = 0x8f

	FormatFixArrayMin Format = 0x90
	FormatFixArrayMax Format = 0x9f

	FormatFixStringMin Format = 0xa0
	FormatFixStringMax Format = 0xbf
)

// Static format codes.
const (
	FormatNil Format = 0xc0 + iota
	_
	FormatFalse
	FormatTrue

	FormatBin8
	FormatBin16
	FormatBin32

	FormatExt8
	FormatExt16
	FormatExt32

	FormatFloat32
	FormatFloat64

	FormatUint8
	FormatUint16
	FormatUint32
	FormatUint64

	FormatInt8
	FormatInt16
	FormatInt32
	FormatInt64

	FormatFixExt1
	FormatFixExt2
	FormatFixExt4
	FormatFixExt8
	FormatFixExt16

	FormatString8
	FormatString16
	FormatString32

	FormatArray16
	FormatArray32

	FormatMap16
	FormatMap32
)

// Format represents format codes of msgpack types.
type Format uint8

// IsFixedNum checks if format is a fixed number value.
func (format Format) IsFixedNum() bool {
	return format <= FormatPositiveFixIntMax || format >= FormatNegativeFixIntMin
}

// IsFixedMap checks if format is a fixed length map.
func (format Format) IsFixedMap() bool {
	return format >= FormatFixMapMin && format <= FormatFixMapMax
}

// IsFixedArray checks if format is a fixed length array.
func (format Format) IsFixedArray() bool {
	return format >= FormatFixArrayMin && format <= FormatFixArrayMax
}

// IsFixedString checks if format is a fixed length string.
func (format Format) IsFixedString() bool {
	return format >= FormatFixStringMin && format <= FormatFixStringMax
}

// IsFixedExt checks if format is a fixed length extension.
func (format Format) IsFixedExt() bool {
	return format >= FormatFixExt1 && format <= FormatFixExt16
}

// IsString checks if format is from string family.
func (format Format) IsString() bool {
	return format == FormatString8 || format == FormatString16 || format == FormatString32 || format.IsFixedString()
}

// IsBin checks if format is from binary family.
func (format Format) IsBin() bool {
	return format == FormatBin8 || format == FormatBin16 || format == FormatBin32
}

// IsArray checks if format is an array.
func (format Format) IsArray() bool {
	return format == FormatArray16 || format == FormatArray32 || format.IsFixedArray()
}

// IsMap checks if format is a map.
func (format Format) IsMap() bool {
	return format == FormatMap16 || format == FormatMap32 || format.IsFixedMap()
}

// IsExt checks if format is from extension family.
func (format Format) IsExt() bool {
	return format == FormatExt8 || format == FormatExt16 || format == FormatExt32 || format.IsFixedExt()
}

// IsPositiveInteger checks if format may represent only positive integer.
func (format Format) IsPositiveInteger() bool {
	return format <= FormatPositiveFixIntMax || (format >= FormatUint8 && format <= FormatUint64)
}

// IsNegativeInteger checks if format may represent negative integer.
func (format Format) IsNegativeInteger() bool {
	return format >= FormatNegativeFixIntMin || (format >= FormatInt8 && format <= FormatInt64)
}

// IsInteger checks if format represents any integer.
func (format Format) IsInteger() bool {
	return format.IsPositiveInteger() || format.IsNegativeInteger()
}

// IsFloat checks if format represents any float.
func (format Format) IsFloat() bool {
	return format == FormatFloat32 || format == FormatFloat64
}
