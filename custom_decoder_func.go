package msgpack

// CustomDecoderFunc defines a function implemented CustomDecoder interface.
type CustomDecoderFunc func(*Decoder) error

// DecodeMsgpack implements CustomDecoder interface.
func (fn CustomDecoderFunc) DecodeMsgpack(dec *Decoder) error {
	return fn(dec)
}
